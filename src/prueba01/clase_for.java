package prueba01;

import javax.swing.JOptionPane;

public class clase_for {
	
	public static void main (String[] args ) {
		
		String[] nombres = new String[3];
		nombres[0] = "juanita";
		nombres[1] = "maria";
		nombres[2] = "carla";
		
		for(int i = 0; i < 3;  i++  ) { /* nunca olvida que al definir una variable, debe definirse su tipo, ya sea
											int, string u otro... en este caso, "int"   */	
			// cualquier opci�n para mostrar, ya sea, ventana de dialogo //1 o impresion en consola //2
			JOptionPane.showInternalMessageDialog(null, nombres[i]);	//1
			System.out.println(nombres[i]);								//2
		}
	
	}
}
