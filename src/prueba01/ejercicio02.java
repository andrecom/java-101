// En una tienda se aplica un 20% de descuento a ni�os, 10%
// para adultos. Tomando en cuenta que se considera ni�o a 
// aquellos que tienen una edad comprendida entre 0-17 a�os.

package prueba01;

import java.util.Scanner;

public class ejercicio02 {
	
	// sentencia que siempre debe ir, porque inicia la aplicaci�nh en Java. Aunque en otros casos
	// esta sentencia se configura en un solo archivo principal del root del package.
	public static void main (String[] args) { 
		
		Scanner entrada = new Scanner(System.in);
		
			double valorfinal;
					
			System.out.println("Ingresa tu edad");
				int edad = entrada.nextInt();
			System.out.println("Ingresa tu el valor de tu compra");
				int vcompra = entrada.nextInt();
				
			if (edad > 17) {
				valorfinal = vcompra-(vcompra*0.10);
			}
			else	{
				valorfinal = vcompra-(vcompra*0.20);
			}
	    
			System.out.println("El valor a pagar es de "+valorfinal);
	}
	
}
