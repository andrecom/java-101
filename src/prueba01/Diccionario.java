package prueba01;

public class Diccionario {

	// Atributos de la clase a los que puedo acceder desde el main (Principal)

	public String[][] capitales = {
			{"Chile","Santiago"},
			{"Venezuela","Caracas"},
			{"Per�","Lima"},
	};
	
/*	private int anioPublicacion = 1996;
	
	public String retornaFecha() {
		return "15 de mayo de "+anioPublicacion;
	}
*/	
	// Declarar el Array tipo String con 5 ciudades y su descripcion

	String [][] ciudades = {
			{"Santiago","Ciudad Capital"},
			{"Valdivia","Valdivia, qu� lindo es Valdivia"},
			{"Concepci�n","Ciudad grande con puentes largos."},
			{"Valpara�so","Va al para�so?"},
			{"Punta Arenas","Sandy Point"},
	};
	
	// M�todo buscar capitales
	public String buscarCapitales(String pais) {
		
		for(int i = 0; i<capitales.length; i++) {
			if(capitales[i][0].equals(pais)) {
				return capitales[i][1];
			}
		}
		
		return "no se encontr� registrado este pa�s";
	}
	
	// M�todo buscar descripcion ciudades
	public String buscarCiudades(String ciudad) { // par�metro ciudad se creado y utiliza dentro del m�todo
			
			for(int i = 0; i<ciudades.length; i++) {
				if(ciudades[i][0].equals(ciudad)) {
					return ciudades[i][1];
				}
			}
			
			return "no se encontr� la ciudad";
		}
	
}




