/* Declarar un arreglo tipo String con ciudades y
 * una descripci�n para cada una de esas ciudades.
 * Recorrer ese arreglo dentro de un for y en funci�n 
 * de la ciudad, vamos a retornar la descripci�n 
 *  */

package prueba01;

import java.util.Scanner;

public class ejer_arreglo_bidim {

		public static void main (String[] args) {
			
			// Declarar el Array tipo String con 5 ciudades y su descripcion
			String [][] ciudades = {
					{"Santiago","Ciudad Capital"},
					{"Valdivia","Valdivia, qu� lindo es Valdivia"},
					{"Concepci�n","Ciudad grande con puentes largos."},
					{"Valpara�so","Va al para�so?"},
					{"Punta Arenas","Sandy Point"},
			};
			
			System.out.println("Seleccione una opci�n:\nConcepci�n\nSantiago\nValdivia\nValpara�so\nPunta Arenas");
				Scanner teclado = new Scanner(System.in);
					String nomcity = teclado.nextLine();
					
				for(int i = 0;i<ciudades.length;i++) {
					
					if(ciudades[i][0].equals(nomcity)){		// funcion .equals() compara entre ciudades y el valor del usuario
						System.out.println(ciudades[i][1]);
					}
				}
			
		}
			
}
