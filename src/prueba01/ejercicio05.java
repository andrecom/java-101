/* mostrar n�meros 0 al 100 de 7 en 7 */
package prueba01;

public class ejercicio05 {
	
	public static void main (String[] args) {
		
		int contador = 0;
	
		while (contador <= 100) {
			
			System.out.printf("%d ",contador);
			++contador;		 //	aumenta de 1 en 1	
			contador +=7;	// aumenta en 7 bajo esa codificaci�n
		}
		
		System.out.println();
	}

}
