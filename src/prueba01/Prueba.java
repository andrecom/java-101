package prueba01;

public class Prueba {
	
	public static void main(String[] args) {
		
		//declaraci�n de variables
		int i = 8;
		int b = 9;
		
		// otras variables
		byte numero1 = 89;
		int numero2 = 88888;
		short numero3 = (short) 46554;
		long numero4 = 96;
		float numero5 = (float) 45645;
		double numero6 = 89.56;
		char caracter = 'a'; // la variable char s�lo puede tener un caracter
		
		// datos no prmitivos
		String nombre = "jos�";
		Integer numero10 = 10;
		
		String otra = nombre.concat(" perez");
		double pi = Math.PI;
		
		System.out.println("Well come gallegos" + otra);
		System.out.println("Segunda y \n \n Tercera l�nea" +numero1);
		System.out.println("\n cuarta l�nea" + numero5);
	}
	
}
