package prueba01;

import javax.swing.JOptionPane;

public class Principal {

	public static void main (String[] args) {
		
		Diccionario dic = new Diccionario();
			
		// System.out.println(dic.ciudades);		// dic: llama al objeto diccionario	 /.ciudades: llama al atributo de la clase
		// System.out.println(dic.anioPublicacion);	// dic: llama al objeto diccionario	 /.anioPublicacion: llama al atributo de la clase 
		// System.out.println(dic.retornaFecha());
	
		// System.out.println(dic.buscarCapitales(pais));
		
		
		/* EJEMPLO
		 * Usuario elija si quiere capital o descripcion de ciudad */

		// 1. Crear el objeto primerusuario de la Clase Usuario con su constructor correspondiente, el cual tiene dos par�metros que se piden a trav�s de una ventana de di�logo
		Usuario primerusuario = new Usuario(JOptionPane.showInputDialog("introduce tu nombre"),JOptionPane.showInputDialog("introduce tu apellido"));
		
		String opcion = JOptionPane.showInputDialog("Se�or "+primerusuario.nombre+" seleccione 1 para ver capital de un pa�s o 2 para ver la descripci�n de una ciudad");
		
		switch(opcion) {
		case "1": 
			String nombrepais = JOptionPane.showInputDialog("Ingrese nombre de pa�s");
			
			String nombrecapital = dic.buscarCapitales(nombrepais);
			JOptionPane.showMessageDialog(null,"sr "+primerusuario.nombre+" ,esta es la capital , "+nombrecapital);
			
		break;
		
		case "2":
			String nombreciudad = JOptionPane.showInputDialog("Ingrese nombre de ciudad");
			
			String descripcionciudad = dic.buscarCiudades(nombreciudad); 
			JOptionPane.showMessageDialog(null,"sr "+primerusuario.nombre+" esto es "+descripcionciudad);
			
		break;
		
		default : 
			JOptionPane.showMessageDialog(null,"introduce el nombre correcto");
		break;
		}
		
		
	}	

}
